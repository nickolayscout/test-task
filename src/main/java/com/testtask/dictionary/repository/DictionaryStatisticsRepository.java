package com.testtask.dictionary.repository;

import com.testtask.dictionary.model.DictionaryStatisticsDTO;
import com.testtask.dictionary.repository.query_builder.DictionaryStatisticsQueryBuilder;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DictionaryStatisticsRepository {

    private final EntityManager entityManager;

    @Transactional
    public DictionaryStatisticsDTO runStatistics() {
        final var queryBuilder = new DictionaryStatisticsQueryBuilder(entityManager);
        return queryBuilder.runStatistics();
    }

}
