package com.testtask.dictionary.repository;

import com.testtask.dictionary.entity.Word;
import com.testtask.dictionary.model.WordDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WordRepository extends JpaRepository<Word, Long> {

    @Query("select w from Word w where w.textEN = :prompt or w.textPL = :prompt")
    Optional<Word> searchByTextENOrTextPL(String prompt);

    @Query("select w.id from Word w where w.textEN = :textEN and w.textPL = :textPL")
    Optional<Long> searchByTextENAndTextPL(String textEN, String textPL);

    @Query("select new com.testtask.dictionary.model.WordDTO(w.id, w.textEN, w.textPL) from Word w")
    Page<WordDTO> listAll(Pageable pageable);
}
