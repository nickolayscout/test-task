package com.testtask.dictionary.repository;

import com.testtask.dictionary.entity.UnknownWord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface UnknownWordRepository extends JpaRepository<UnknownWord, Long> {

    @Query("select w.id from UnknownWord w where w.text = :text")
    Optional<Long> findByText(String text);

    @Query("select w.text from UnknownWord w ")
    Page<String> listAll(Pageable pageable);

    @Query("select w.id from UnknownWord w where w.text in :text")
    List<Long> findAllByText(Collection<String> text);

}
