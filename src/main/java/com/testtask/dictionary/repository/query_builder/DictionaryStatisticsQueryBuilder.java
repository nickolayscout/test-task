package com.testtask.dictionary.repository.query_builder;

import com.testtask.dictionary.entity.UnknownWord;
import com.testtask.dictionary.entity.Word;
import com.testtask.dictionary.model.DictionaryStatisticsDTO;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.*;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class DictionaryStatisticsQueryBuilder {

    private final EntityManager em;
    private final CriteriaBuilder criteriaBuilder;

    public DictionaryStatisticsQueryBuilder(EntityManager em) {
        this.em = em;
        this.criteriaBuilder = em.getCriteriaBuilder();
    }

    public DictionaryStatisticsDTO runStatistics() {
        final var wordsLength = new ArrayList<DictionaryStatisticsDTO.Length>();
        wordsLength.addAll(selectLanguageStats("EN", "textEN"));
        wordsLength.addAll(selectLanguageStats("PL", "textPL"));

        final var avgStats = new ArrayList<DictionaryStatisticsDTO.Average>();
        avgStats.addAll(selectAverageStats("EN", "textEN"));
        avgStats.addAll(selectAverageStats("PL", "textPL"));

        final var knownWords = countKnownWords();
        final var unknownWords = countUnknownWords();
        return new DictionaryStatisticsDTO(
                knownWords,
                unknownWords,
                wordsLength,
                avgStats
        );
    }

    private Long countUnknownWords() {
        final var query = criteriaBuilder.createQuery(Long.class);
        final var root = query.from(UnknownWord.class);
        final var count = criteriaBuilder.count(root);
        query.select(count);
        return em.createQuery(query).getSingleResult();
    }

    private Long countKnownWords() {
        final var query = criteriaBuilder.createQuery(Long.class);
        final var root = query.from(Word.class);
        final var count = criteriaBuilder.count(root);
        query.select(count);
        return em.createQuery(query).getSingleResult();
    }

    private List<DictionaryStatisticsDTO.Length> selectLanguageStats(String languageAlias, String field) {
        final var clazz = DictionaryStatisticsDTO.Length.class;
        final var query = criteriaBuilder.createQuery(clazz);
        final var root = query.from(Word.class);
        final var language = criteriaBuilder.literal(languageAlias);
        final var length = criteriaBuilder.length(root.get(field));
        final var count = criteriaBuilder.count(root.get("id"));
        query.groupBy(length);
        final var selection = criteriaBuilder.construct(clazz, language, length, count);
        query.select(selection);
        return em.createQuery(query).getResultList();
    }

    private List<DictionaryStatisticsDTO.Average> selectAverageStats(String languageAlias, String field) {
        final var clazz = DictionaryStatisticsDTO.Average.class;
        final var query = criteriaBuilder.createQuery(clazz);
        final var root = query.from(Word.class);
        final var language = criteriaBuilder.literal(languageAlias);
        final var length = criteriaBuilder.length(root.get(field));
        final var avg = criteriaBuilder.avg(length);
        final var selection = criteriaBuilder.construct(clazz, language, avg);
        query.select(selection);
        return em.createQuery(query).getResultList();
    }

}
