package com.testtask.dictionary.mapper;

import com.testtask.dictionary.entity.Word;
import com.testtask.dictionary.model.CreateWordDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface WordMapper {

    Word fromDTO(CreateWordDTO word);

}
