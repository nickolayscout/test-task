package com.testtask.dictionary.mapper;

import com.testtask.dictionary.model.DictionaryStatisticsDTO;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class StatisticsMapper {

    public Map<String, Object> toMap(DictionaryStatisticsDTO dto) {
        var map = new HashMap<String, Object>();
        map.put("knownWords", dto.getKnownWords());
        map.put("unknownWords", dto.getUnknownWords());

        var lengthList = mapLengths(dto.getWordsLength());
        map.put("wordsLength", lengthList);

        var averageList = mapAverages(dto.getWordsLengthAverage());
        map.put("wordsLengthAverage", averageList);

        return map;
    }

    private List<Map<String, Object>> mapLengths(List<DictionaryStatisticsDTO.Length> lengths) {
        return lengths.stream().map(l -> {
            Map<String, Object> lengthMap = new HashMap<>();
            lengthMap.put("language", l.language());
            lengthMap.put("length", l.length());
            lengthMap.put("count", l.count());
            return lengthMap;
        }).toList();
    }

    private List<Map<String, Object>> mapAverages(List<DictionaryStatisticsDTO.Average> averages) {
        return averages.stream().map(a -> {
            Map<String, Object> averageMap = new HashMap<>();
            averageMap.put("language", a.language());
            averageMap.put("average", a.average());
            return averageMap;
        }).toList();
    }

}
