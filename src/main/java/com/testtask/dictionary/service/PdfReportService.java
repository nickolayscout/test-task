package com.testtask.dictionary.service;

import com.lowagie.text.DocumentException;
import com.testtask.dictionary.mapper.StatisticsMapper;
import com.testtask.dictionary.model.DictionaryStatisticsDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.OutputStream;

@Service
@RequiredArgsConstructor
public class PdfReportService {

    private final StatisticsService statisticsService;
    private final TemplateEngine templateEngine;
    private final StatisticsMapper mapper;

    public void generatePdfReport(OutputStream os) throws DocumentException {
        final var statistics = statisticsService.runStatistics();
        final Context context = getContext(statistics);
        final var reportHTML = templateEngine.process("report", context);
        generatePdfFromHtml(reportHTML, os);
    }

    private Context getContext(DictionaryStatisticsDTO statistics) {
        final var map = mapper.toMap(statistics);
        final var context = new Context();
        context.setVariables(map);
        return context;
    }


    private void generatePdfFromHtml(String html, OutputStream os) throws DocumentException {
        var renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(os);

    }
}
