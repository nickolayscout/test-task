package com.testtask.dictionary.service;

import com.testtask.dictionary.entity.Word;
import com.testtask.dictionary.holder.RegexHolder;
import com.testtask.dictionary.mapper.WordMapper;
import com.testtask.dictionary.model.CreateWordDTO;
import com.testtask.dictionary.model.WordDTO;
import com.testtask.dictionary.repository.WordRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class WordService {

    private final WordRepository repository;
    private final WordMapper mapper;
    private final UnknownWordService unknownWordService;

    private static final Pattern SINGLE_WORD = Pattern.compile(RegexHolder.WORD);

    @Transactional
    public void saveNew(CreateWordDTO wordDTO) {
        final var word = mapper.fromDTO(wordDTO);
        if (exists(wordDTO.getTextEN(), word.getTextPL())) {
            return;
        }
        repository.save(word);
        removeFromUnknowns(word);
    }

    private void removeFromUnknowns(Word saved) {
        final var text = Arrays.asList(saved.getTextPL(), saved.getTextEN());
        unknownWordService.removeByText(text);
    }

    private boolean exists(String textEN, String textPL) {
        final var id = repository.searchByTextENAndTextPL(textEN, textPL);
        return id.isPresent();
    }

    @Transactional
    public String translateSentence(String prompt) {
        final var matcher = SINGLE_WORD.matcher(prompt);
       return matcher.replaceAll(this::matchResult);
    }

    @Transactional
    protected String matchResult(MatchResult matchResult) {
        final var word = matchResult.group();
        return translate(word).orElse(word);
    }

    @Transactional
    public Optional<String> translate(String prompt) {
        final var optional = repository.searchByTextENOrTextPL(prompt)
                .map(word -> extractTranslation(prompt, word));

        if (optional.isEmpty()) {
            handleUnknownWord(prompt);
        }
        return optional;
    }

    private void handleUnknownWord(String prompt) {
        unknownWordService.save(prompt);
    }

    private String extractTranslation(String prompt, Word word) {
        if (word.getTextEN().equals(prompt)) {
            return word.getTextPL();
        } else {
            return word.getTextEN();
        }
    }

    public Page<WordDTO> listWords(Pageable pageable) {
        return repository.listAll(pageable);
    }
}
