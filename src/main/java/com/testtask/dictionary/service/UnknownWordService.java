package com.testtask.dictionary.service;

import com.testtask.dictionary.entity.UnknownWord;
import com.testtask.dictionary.repository.UnknownWordRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class UnknownWordService {

    private final UnknownWordRepository repository;

    @Transactional
    public void save(String text) {
        if (exists(text)) {
            return;
        }
        final var word = new UnknownWord();
        word.setText(text);
        repository.save(word);
    }

    private boolean exists(String text) {
        return repository.findByText(text).isPresent();
    }

    @Transactional
    public void removeByText(Collection<String> text) {
        //Because of unique constraint on the 'text' column in DB,
        //we don't really need pagination here, since ids.size() won't be bigger than input size
        final var ids = repository.findAllByText(text);
        repository.deleteAllById(ids);
    }

    public Page<String> listPage(Pageable pageable) {
        return repository.listAll(pageable);
    }

}
