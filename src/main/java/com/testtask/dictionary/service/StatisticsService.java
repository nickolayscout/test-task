package com.testtask.dictionary.service;

import com.testtask.dictionary.model.DictionaryStatisticsDTO;
import com.testtask.dictionary.repository.DictionaryStatisticsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StatisticsService {

    private final DictionaryStatisticsRepository repository;

    public DictionaryStatisticsDTO runStatistics() {
        return repository.runStatistics();
    }

}
