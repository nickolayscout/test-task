package com.testtask.dictionary.model;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WordDTO {

    private Long id;

    private String textEN;

    private String textPL;

}
