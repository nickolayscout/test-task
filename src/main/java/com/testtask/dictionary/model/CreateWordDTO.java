package com.testtask.dictionary.model;

import com.testtask.dictionary.holder.RegexHolder;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;


@Data
public class CreateWordDTO {

    @NotNull
    @NotBlank
    @Pattern(regexp = RegexHolder.WORD)
    private String textPL;

    @NotNull
    @NotBlank
    @Pattern(regexp = RegexHolder.WORD)
    private String textEN;

}
