package com.testtask.dictionary.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class DictionaryStatisticsDTO {
    /*
    *
    * the number of words in the dictionary, the number of words of
each length (separately for each language), the average length of all words (separately for each
language), and the number of words to be completed.
    * */

    private Long knownWords;
    private Long unknownWords;
    private List<Length> wordsLength;
    private List<Average> wordsLengthAverage;

    public record Length(String language, Integer length, Long count) {
    }

    public record Average(String language, Double average) {

    }

}
