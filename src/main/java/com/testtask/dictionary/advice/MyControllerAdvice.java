package com.testtask.dictionary.advice;

import com.testtask.dictionary.holder.ProblemHolder;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Log4j2
public class MyControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> handleValidationException(MethodArgumentNotValidException ex) {
        return ResponseEntity.of(ex.getBody()).build();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handleServerException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return ResponseEntity.of(ProblemHolder.GENERIC_ERROR).build();
    }

}
