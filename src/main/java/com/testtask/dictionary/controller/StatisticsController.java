package com.testtask.dictionary.controller;

import com.testtask.dictionary.model.DictionaryStatisticsDTO;
import com.testtask.dictionary.service.PdfReportService;
import com.testtask.dictionary.service.StatisticsService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/statistics")
@RequiredArgsConstructor
public class StatisticsController {

    private final StatisticsService statisticsService;
    private final PdfReportService reportService;

    @GetMapping
    public DictionaryStatisticsDTO runStatistics() {
        return statisticsService.runStatistics();
    }

    @GetMapping("/report")
    public void generateReport(HttpServletResponse response) throws Exception {
        reportService.generatePdfReport(response.getOutputStream());
    }

}
