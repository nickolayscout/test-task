package com.testtask.dictionary.controller;

import com.testtask.dictionary.holder.ProblemHolder;
import com.testtask.dictionary.model.CreateWordDTO;
import com.testtask.dictionary.model.WordDTO;
import com.testtask.dictionary.service.UnknownWordService;
import com.testtask.dictionary.service.WordService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/dictionary")
@RequiredArgsConstructor
public class DictionaryController {

    private final WordService wordService;
    private final UnknownWordService unknownWordService;

    @PostMapping
    public void saveWord(@Valid @RequestBody CreateWordDTO wordDTO) {
        wordService.saveNew(wordDTO);
    }

    @GetMapping("/translate")
    public ResponseEntity<String> translate(@RequestParam String prompt) {
        final var translation = wordService.translate(prompt);
        return translation.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.of(ProblemHolder.WORD_NOT_FOUND).build());
    }

    @GetMapping("/translate-sentence")
    public ResponseEntity<String> translateSentence(@RequestParam String prompt) {
        final var sentence = wordService.translateSentence(prompt);
        return ResponseEntity.ok(sentence);
    }

    @GetMapping("/list")
    public Page<WordDTO> listWords(@RequestParam int page,
                                   @RequestParam int size) {
        return wordService.listWords(PageRequest.of(page, size));
    }

    @GetMapping("/list-unknown")
    public Page<String> listUnknownWords(@RequestParam int page,
                                   @RequestParam int size) {
        return unknownWordService.listPage(PageRequest.of(page, size));
    }
}
