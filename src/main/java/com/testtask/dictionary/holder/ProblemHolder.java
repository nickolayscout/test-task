package com.testtask.dictionary.holder;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;

public interface ProblemHolder {

    ProblemDetail WORD_NOT_FOUND = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, "Unexpected Prompt");
    ProblemDetail GENERIC_ERROR = ProblemDetail.forStatusAndDetail(HttpStatus.INTERNAL_SERVER_ERROR, "Server error");
}
