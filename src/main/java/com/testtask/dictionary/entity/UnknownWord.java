package com.testtask.dictionary.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "unknown_word")
@Data
public class UnknownWord {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "text")
    private String text;

}
