package com.testtask.dictionary.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "text_en")
    private String textEN;

    @Column(name = "text_pl")
    private String textPL;

}

