package com.testtask.dictionary.controller;

import jakarta.transaction.Transactional;
import org.hamcrest.number.IsCloseTo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class StatisticsControllerTest {

    @Autowired
    private MockMvc mvc;

    private static final double EPS = 0.0001;

    @Transactional
    void setUp() throws Exception {
        //language=JSON
        final var payload1 = """
                {
                    "textEN" : "FOO",
                    "textPL" : "BAR"
                }
                """;
        //language=JSON
        final var payload2 = """
                {
                    "textEN" : "FEE",
                    "textPL" : "BARRR"
                }
                """;
        //language=JSON
        final var payload3 = """
                {
                    "textEN" : "FO",
                    "textPL" : "BAEEE"
                }
                """;
        mvc.perform(post("/dictionary")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload1));
        mvc.perform(post("/dictionary")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload2));
        mvc.perform(post("/dictionary")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload3));
    }

    @Test

    void runStatistics() throws Exception {
        setUp();
        mvc.perform(get("/statistics"))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("knownWords").value(3),
                        jsonPath("unknownWords").value(0),
                        jsonPath("wordsLength[0].language").value("EN"),
                        jsonPath("wordsLength[0].length").value(2),
                        jsonPath("wordsLength[0].count").value(1),
                        jsonPath("wordsLength[1].language").value("EN"),
                        jsonPath("wordsLength[1].length").value(3),
                        jsonPath("wordsLength[1].count").value(2),
                        jsonPath("wordsLength[2].language").value("PL"),
                        jsonPath("wordsLength[2].length").value(3),
                        jsonPath("wordsLength[2].count").value(1),
                        jsonPath("wordsLength[3].language").value("PL"),
                        jsonPath("wordsLength[3].length").value(5),
                        jsonPath("wordsLength[3].count").value(2),

                        jsonPath("wordsLengthAverage[0].language").value("EN"),
                        jsonPath("wordsLengthAverage[0].average").value(IsCloseTo.closeTo(2.666666666666666, EPS)),
                        jsonPath("wordsLengthAverage[1].language").value("PL"),
                        jsonPath("wordsLengthAverage[1].average").value(IsCloseTo.closeTo(4.333333333333333, EPS))
                );
    }

    @Test
    void generatePdfReport() throws Exception {
        setUp();
        mvc.perform(get("/statistics/report"))
                .andExpectAll(
                        status().isOk()
                      );
    }
}