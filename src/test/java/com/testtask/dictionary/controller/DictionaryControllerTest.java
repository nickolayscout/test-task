package com.testtask.dictionary.controller;

import com.testtask.dictionary.entity.Word;
import com.testtask.dictionary.model.WordDTO;
import com.testtask.dictionary.repository.UnknownWordRepository;
import com.testtask.dictionary.repository.WordRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class DictionaryControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private WordRepository mockWordRepository;

    @MockBean
    private UnknownWordRepository mockUnknownWordRepository;

    @Test
    void saveWord() throws Exception {
        //language=JSON
        final var payload = """
                {
                    "textEN" : "FOO",
                    "textPL" : "BAR"
                }
                """;
        mvc.perform(post("/dictionary")
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload))
                .andExpect(status().isOk());

    }

    @Test
    void saveWord_incorrect_1() throws Exception {
        //language=JSON
        final var payload = """
                {
                    "textEN" : "   FOO",
                    "textPL" : "BAR"
                }
                """;
        mvc.perform(post("/dictionary")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(payload))
                .andExpect(status().isBadRequest());

    }

    @Test
    void saveWord_incorrect_2() throws Exception {
        //language=JSON
        final var payload = """
                {
                    "textEN" : "$$FO0",
                    "textPL" : "BAR"
                }
                """;
        mvc.perform(post("/dictionary")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(payload))
                .andExpect(status().isBadRequest());

    }

    @Test
    void translate() throws Exception {
        final var mockWord = new Word();
        mockWord.setId(-1L);
        mockWord.setTextPL("FOO");
        mockWord.setTextEN("BAR");
        when(mockWordRepository.searchByTextENOrTextPL("FOO")).thenReturn(Optional.of(mockWord));
        mvc.perform(get("/dictionary/translate").param("prompt", "FOO"))
                .andExpectAll(
                        status().isOk(),
                        content().string("BAR")
                );
    }

    @Test
    void translate_not_found() throws Exception {
        when(mockWordRepository.searchByTextENOrTextPL("FOO")).thenReturn(Optional.empty());
        mvc.perform(get("/dictionary/translate").param("prompt", "FOO"))
                .andExpectAll(
                        status().isNotFound(),
                        jsonPath("detail").value("Unexpected Prompt")
                );
    }

    @Test
    void translateSentence() throws Exception {
        final var mockWord1 = new Word();
        mockWord1.setId(-1L);
        mockWord1.setTextPL("FOO");
        mockWord1.setTextEN("BAR");

        final var mockWord2 = new Word();
        mockWord2.setId(-2L);
        mockWord2.setTextPL("FIZZ");
        mockWord2.setTextEN("BUZZ");


        when(mockWordRepository.searchByTextENOrTextPL("FOO")).thenReturn(Optional.of(mockWord1));
        when(mockWordRepository.searchByTextENOrTextPL("FIZZ")).thenReturn(Optional.of(mockWord2));
        when(mockWordRepository.searchByTextENOrTextPL("PIZZA")).thenReturn(Optional.empty());

        mvc.perform(get("/dictionary/translate-sentence")
                .param("prompt", "FOO FIZZ PIZZA 000 $$$"))
                .andExpectAll(
                        status().isOk(),
                        content().string("BAR BUZZ PIZZA 000 $$$")
                );
    }

    @Test
    void listWords() throws Exception {
        final int EXPECTED_PAGE= 0;
        final int EXPECTED_SIZE = 10;

        final var words = Arrays.asList(
                new WordDTO(1L, "FOO", "BAR"),
                new WordDTO(2L, "I_LOVE", "DONUTS"),
                new WordDTO(3L, "I_WANT", "PIZZA")
        );

        final var mockPage = new PageImpl<>(words, PageRequest.of(EXPECTED_PAGE, EXPECTED_SIZE), 3L);
        when(mockWordRepository.listAll(argThat(pageable -> pageable.getPageSize() == EXPECTED_SIZE
                && pageable.getPageNumber() == EXPECTED_PAGE))).thenReturn(mockPage);

        mvc.perform(get("/dictionary/list")
                .param("page", Integer.toString(EXPECTED_PAGE))
                .param("size", Integer.toString(EXPECTED_SIZE)))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("content[0].id").value(1),
                        jsonPath("content[0].textEN").value("FOO"),
                        jsonPath("content[0].textPL").value("BAR"),
                        jsonPath("content[1].id").value(2),
                        jsonPath("content[1].textEN").value("I_LOVE"),
                        jsonPath("content[1].textPL").value("DONUTS"),
                        jsonPath("content[2].id").value(3),
                        jsonPath("content[2].textEN").value("I_WANT"),
                        jsonPath("content[2].textPL").value("PIZZA"),
                        jsonPath("pageable.pageNumber").value(0),
                        jsonPath("pageable.pageSize").value(10),
                        jsonPath("totalElements").value(3)
                );

    }

    @Test
    void listUnknownWords() throws Exception {
        final int EXPECTED_PAGE= 0;
        final int EXPECTED_SIZE = 10;
        final var words = Arrays.asList(
                "FOO", "BAR"
        );

        final var mockPage = new PageImpl<>(words, PageRequest.of(EXPECTED_PAGE, EXPECTED_SIZE), 3L);
        when(mockUnknownWordRepository.listAll(argThat(pageable -> pageable.getPageSize() == EXPECTED_SIZE
                && pageable.getPageNumber() == EXPECTED_PAGE))).thenReturn(mockPage);

        mvc.perform(get("/dictionary/list-unknown")
                        .param("page", Integer.toString(EXPECTED_PAGE))
                        .param("size", Integer.toString(EXPECTED_SIZE)))
                .andExpectAll(
                        status().isOk(),
                        jsonPath("content[0]").value("FOO"),
                        jsonPath("content[1]").value("BAR"),
                        jsonPath("pageable.pageNumber").value(0),
                        jsonPath("pageable.pageSize").value(10),
                        jsonPath("totalElements").value(2)
                );

    }
}